import { LogLevelDesc } from 'loglevel'

import packageJson from '../package.json'

export const CONFIG = {
  API_URL: import.meta.env.VITE_APP_API_URL,
  APP_NAME: 'web-client-react-template',
  LOG_LEVEL: 'trace' as LogLevelDesc,
  BUILD_VERSION: packageJson.version || import.meta.env.VITE_APP_BUILD_VERSION,
} as const

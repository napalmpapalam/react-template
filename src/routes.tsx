import { lazy, Suspense } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

import { RoutePaths } from '@/enums'
import Home from '@/pages/Home'

const AppRoutes = () => {
  const About = lazy(() => import('@/pages/About'))

  // TODO: Add fallback
  return (
    <Suspense fallback={<> </>}>
      <Routes>
        <Route path={RoutePaths.Home} element={<Home />} />
        <Route path={RoutePaths.About} element={<About />} />
        <Route path='*' element={<Navigate replace to={RoutePaths.Home} />} />
      </Routes>
    </Suspense>
  )
}

export default AppRoutes

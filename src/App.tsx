import { useTranslation } from 'react-i18next'

import { useViewportSizes } from '@/hooks'
import AppRoutes from '@/routes'

const App = () => {
  useViewportSizes()

  const { t } = useTranslation()

  return (
    <div className='App'>
      <p>{t('app.welcome-title')}</p>
      <AppRoutes />
    </div>
  )
}

export default App

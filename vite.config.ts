import react from '@vitejs/plugin-react'
import { defineConfig } from 'vite'
import checker from 'vite-plugin-checker'
import tsconfigPaths from 'vite-tsconfig-paths'

export default defineConfig({
  publicDir: 'static',
  server: {
    port: 8080,
  },
  plugins: [
    react(),
    tsconfigPaths(),
    checker({
      typescript: true,
      eslint: {
        lintCommand: 'eslint "./src/**/*.{ts,tsx}"',
      },
    }),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "src/styles/_mixins.scss";
          @import "src/styles/_placeholders.scss";
          @import "src/styles/_functions.scss";
          @import "src/styles/_media.scss";
        `,
      },
    },
  },
})

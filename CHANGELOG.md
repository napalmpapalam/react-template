# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0-rc.1] - 2022-03-23
#### Under the hood changes
- Initiated and setup project

[Unreleased]: https://gitlab.com/napalmpapalam/react-template/compare/1.0.0-rc.1...main
[1.0.0-rc.1]: https://gitlab.com/napalmpapalam/react-template/tags/1.0.0-rc.1

import react from '@vitejs/plugin-react'
import { visualizer } from 'rollup-plugin-visualizer'
import { defineConfig } from 'vite'
import tsconfigPaths from 'vite-tsconfig-paths'

export default defineConfig({
  publicDir: 'static',
  server: {
    port: 8080,
  },
  plugins: [
    react(),
    tsconfigPaths(),
    visualizer({
      open: true,
      title: 'web-client-react-template bundle analyzer',
    }),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "src/styles/_mixins.scss";
          @import "src/styles/_placeholders.scss";
          @import "src/styles/_functions.scss";
          @import "src/styles/_media.scss";
        `,
      },
    },
  },
})

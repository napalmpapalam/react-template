# web-client-react-template

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn start
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Lints release/release candidate version
```
yarn rsc %release-version%
```


Search in project files and replace `web-client-react-template` with your project name
